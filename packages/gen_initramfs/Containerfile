FROM scratch as base
ENV VERSION=6.6
ENV SRC_HASH=d926a06c63dd8ac7df3f86ee1ffc2ce2a3b81a2d168484e76b5b389aba8e56d0
ENV SRC_FILE=linux-${VERSION}.tar.xz
ENV SRC_SITE=http://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stagex/busybox . /
COPY --from=stagex/gcc . /
COPY --from=stagex/binutils . /
COPY --from=stagex/musl . /
RUN tar -xf ${SRC_FILE}
WORKDIR linux-${VERSION}
RUN --network=none gcc usr/gen_init_cpio.c -o usr/gen_init_cpio

FROM build as install
RUN --network=none <<-EOF
    set -eux
    mkdir -p /rootfs/usr/bin
    cat usr/gen_initramfs.sh \
        | sed 's:usr/gen_init_cpio:gen_init_cpio:g' \
        > /rootfs/usr/bin/gen_initramfs
    chmod +x /rootfs/usr/bin/gen_initramfs
    cp -a usr/gen_init_cpio /rootfs/usr/bin
EOF
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
