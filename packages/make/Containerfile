FROM scratch as base
ENV VERSION=4.4
ENV SRC_FILE=make-${VERSION}.tar.gz
ENV SRC_SITE=https://ftp.gnu.org/gnu/make/${SRC_FILE}
ENV SRC_HASH=581f4d4e872da74b3941c874215898a7d35802f03732bdccee1d4a7979105d18

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stagex/stage3 . /
RUN tar -xf ${SRC_FILE}
WORKDIR make-${VERSION}
RUN --network=none <<-EOF
	set -eux
	./configure \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-nls
	make
EOF

FROM build as install
RUN --network=none make DESTDIR="/rootfs" install
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
