FROM scratch as base
ENV VERSION=1.3.1
ENV SRC_HASH=9a93b2b7dfdac77ceba5a558a580e74667dd6fede4585b91eefb60f03b72df23
ENV SRC_FILE=zlib-${VERSION}.tar.gz
ENV SRC_SITE=https://www.zlib.net/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stagex/busybox . /
COPY --from=stagex/gcc . /
COPY --from=stagex/binutils . /
COPY --from=stagex/make . /
COPY --from=stagex/musl . /
RUN tar -xf ${SRC_FILE}
WORKDIR zlib-${VERSION}
RUN --network=none <<-EOF
	set -eux; \
	./configure \
		--prefix=/usr \
		--libdir=/lib \
		--shared; \
	make
EOF

FROM build as install
RUN make DESTDIR=/rootfs install
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
